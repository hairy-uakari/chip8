const {ipcRenderer} = require('electron');
const Chip8 = require('./chip8');
let emulator = new Chip8();

const keymap = [49, 50, 51, 52, 81, 87, 69, 82, 65, 83, 68, 70, 90, 88, 67, 86];
let context = null;
let color = ['blue', 'red'];
let current = 0;

function draw() {
	emulator.draw = false;
	for(let i = 0; i < 32; i++)
		for(let j = 0; j < 64; j++) {
			if(emulator.display[j + i * 64] === 1) {
				context.fillStyle = 'white';
				context.fillRect(j * 10, i * 10, 10, 10);
			}
		}
}

let lastTime = 0;

function run() {
	emulator.emulateCycle();
	if(context != null && emulator.draw) {
		context.fillStyle = 'blue';
		context.fillRect(0, 0, 640, 320);
		draw();
	}
	window.requestAnimationFrame(run);
}

function init() {
	context = document.querySelector('#display').getContext('2d');
	window.addEventListener('keydown', (event) => {
		for(let i = 0; i < keymap.length; i++) 
			if(keymap[i] == event.keyCode) {
				emulator.keyboard[i] = 1;
			}
	});

	window.addEventListener('keyup', (event) => {
		for(let i = 0; i < keymap.length; i++)
			if(keymap[i] == event.keyCode)
				emulator.keyboard[i] = 0;
	});

	context.fillStyle = 'black';
	context.fillRect(0, 0, 640, 320);
	ipcRenderer.on('rom-loaded', function(event, data) {
		emulator.memory = data;
		window.requestAnimationFrame(run);
		setInterval(function() {
			if(emulator.delayTimer > 0) emulator.delayTimer--;
			if(emulator.soundTimer > 0) --emulator.soundTimer;
		}, 1000 / 60);
	});
}

