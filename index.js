const {app, BrowserWindow, Menu} = require('electron');
const Chip8 = require('./chip8');
const emulator = new Chip8();

let a = []

function loadROM(file, emu) {
	const fs = require('fs');
	fs.open(file, 'r', (err, res) => {
		if(err) throw err;
		let buffer = new Buffer.alloc(1);
		let i = 0;
		while(true) {
			let num = fs.readSync(res, buffer, 0, 1, null);
			if(num === 0) break;
			a[i + 512] = buffer[0];
			++i;
		}
	});
}

let win = null;

function createWindow() {
	 win = new BrowserWindow({
		width: 800,
		height: 600,
		webPreferences: {
			nodeIntegration: true
		}
	});

	win.loadFile('index.html');
	win.webContents.openDevTools();
}


let menu = Menu.buildFromTemplate([
	{
		label: 'Menu',
		submenu: [
			{
				label: 'Open ROM',
				click() {
					const dialog = require('electron').dialog;
					const fs = require('fs');
					const {ipcMain} = require('electron');

					dialog.showOpenDialog(function(files) {
						if(files === undefined)
							console.log("No files selected");
						else {
							fs.open(files[0], 'r', (err, fd) => {
								if(err) throw err;
								let buffer = new Buffer.alloc(1);
								let i = 0;
								while(true) {
									let num = fs.readSync(fd, buffer, 0, 1, null);
									if(num === 0) break;
									if((i + 512) > 4095) throw "ROM TOO BIG";
									emulator.memory[i + 512] = buffer[0];
									++i;
								}
								win.webContents.send('rom-loaded', emulator.memory);
							});
						}
					});
				}
			},
			{
				label: 'Exit',
				click() {
					app.quit();
				}
			}
		]
	}
]);

Menu.setApplicationMenu(menu);

app.on('ready', createWindow);
