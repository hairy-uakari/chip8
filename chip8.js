class Chip8 {
	constructor() {
		this.pc = 512;
		this.memory = new Array(4096).fill(0);
		this.v = new Array(16).fill(0);
		this.i = 0;
		this.display = new Array(64  * 32).fill(0);
		this.soundTimer = 0;
		this.delayTimer = 0;
		this.stack = new Array(16).fill(0);
		this.sp = 0;
		this.keyboard = new Array(16).fill(0);
		this.draw = false;
		let font = [
					0xF0, 0x90, 0x90, 0x90, 0xF0, // 0
					0x20, 0x60, 0x20, 0x20, 0x70, // 1
					0xF0, 0x10, 0xF0, 0x80, 0xF0, // 2
					0xF0, 0x10, 0xF0, 0x10, 0xF0, // 3
					0x90, 0x90, 0xF0, 0x10, 0x10, // 4
					0xF0, 0x80, 0xF0, 0x10, 0xF0, // 5
					0xF0, 0x80, 0xF0, 0x90, 0xF0, // 6
					0xF0, 0x10, 0x20, 0x40, 0x40, // 7
					0xF0, 0x90, 0xF0, 0x90, 0xF0, // 8
					0xF0, 0x90, 0xF0, 0x10, 0xF0, // 9
					0xF0, 0x90, 0xF0, 0x90, 0x90, // A
					0xE0, 0x90, 0xE0, 0x90, 0xE0, // B
					0xF0, 0x80, 0x80, 0x80, 0xF0, // C
					0xE0, 0x90, 0x90, 0x90, 0xE0, // D
					0xF0, 0x80, 0xF0, 0x80, 0xF0, // E
					0xF0, 0x80, 0xF0, 0x80, 0x80 // F
		];

		for(let i = 0; i < 80; i++)
			this.memory[i] = font[i];
	}

	emulateCycle() {
		let opcode = (this.memory[this.pc] << 8) | this.memory[this.pc + 1];
		let x = (opcode & 0x0F00) >> 8;
		let y = (opcode & 0x00F0) >> 4;
		this.pc += 2;
		switch(opcode & 0xF000) {
			case 0x0000:
				switch(opcode & 0x000F) {
					case 0x0000:
						this.display.fill(0);
						this.draw = true;
						break;
					case 0x000E:
						--this.sp;
						this.pc = this.stack[this.sp];
						break;
					default:
						console.error(`Invalid opcode -> ${opcode}`);
				}
				break;
			case 0x1000:
			this.pc = opcode & 0x0FFF;
				break;
			case 0x2000:
				this.stack[this.sp++] = this.pc;
				this.pc = (opcode & 0x0FFF);
				break;
			case 0x3000:
				if(this.v[x] === (opcode & 0x00FF))
					this.pc += 2;
				break;
			case 0x4000:
				if(this.v[x] != (opcode & 0x00FF))
					this.pc += 2;
				break;
			case 0x5000:
				if(this.v[x] === this.v[y])
					this.pc += 2;
				break;
			case 0x6000:
				this.v[x] = opcode & 0x00FF;
				break;
			case 0x7000:
				this.v[0xF] = 0;
				this.v[x] += opcode & 0x00FF;
				if(this.v[x] > 0xFF)
					this.v[x] -= 256;
				break;
			case 0x8000:
				switch(opcode & 0x000F) {
					case 0x0000:
						this.v[x] = this.v[y];
						break;
					case 0x0001:
						this.v[x] |= this.v[y];
						break;
					case 0x0002:
						this.v[x] &= this.v[y];
						break;
					case 0x0003:
						this.v[x] ^= this.v[y];
						break;
					case 0x0004:
						this.v[0xF] = 0;
						this.v[x] += this.v[y];
						if(this.v[x] > 0xFF) {
							this.v[x] -= 256;
							this.v[0xF] = 1;
						}
						break;
					case 0x0005:
						this.v[0xF] = 0;
						this.v[x] -= this.v[y];
						if(this.v[x] < 0) {
							this.v[x] += 256;
							this.v[0xF] = 1;
						}
						break;
					case 0x0006:
						this.v[0xF] = this.v[x] & 1;
						this.v[x] >>= 1;
						break;
					case 0x0007:
						this.v[0xF] = 0;
						this.v[x] = this.v[y] - this.v[x];
						if(this.v[x] < 0) {
							this.v[0xF] = 1;
							this.v[x] += 256;
						}
						break;
					case 0x000E:
						this.v[0xF] = this.v[x] & (1 << 7);
						this.v[x] <<= 1;
						break;
					default:
						console.error(`Invalid opcode ${opcode}`);
				}
				break;
			case 0x9000:
				if(this.v[x] != this.v[y])
					this.pc += 2;
				break;
			case 0xA000:
				this.i = opcode & 0x0FFF;
				break;
			case 0xB000:
				this.pc = this.v[0] + (opcode & 0x0FFF);
				break;
			case 0xC000:
				this.v[x] = Math.floor(Math.random() * 0xFF) & (opcode & 0xFF)
				break;
			case 0xD000:
				{
					this.draw = true;
					let xc = this.v[x], yc = this.v[y];
					let height = opcode & 0x000F;
					this.v[0xF] = 0;
					for(let i = 0; i < height; i++) {
						let current = this.memory[this.i + i];
						for(let j = 0; j < 8; j++) {
							const val = (current >> (7 - j)) & 0b00000001;
							const index = ((xc + j) + ((yc + i) * 64)) % 2048;
							const prev = this.display[index];
							const newVal = val ^ prev;
							this.display[index] = newVal;
							if(this.display[index] == 0 && prev === 1)
								this.v[0xF] = 1;
						}
						console.log("CALLED");
					}
				}
				break;
			case 0xE000:
				switch(opcode & 0x00FF) {
					case 0x009E:
						if(this.keyboard[this.v[x]] === 1)
							this.pc += 2;
						break;
					case 0x00A1:
						if(this.keyboard[this.v[x]] === 0)
							this.pc += 2;
						break;
				}
				break;
			case 0xF000:
				switch(opcode & 0x00FF) {
					case 0x0007:
						this.v[x] = this.delayTimer;
						break;
					case 0x000A:
						this.pc -= 2;
						let pressed = false;
						for(let i = 0; i < 16; i++)
							if(this.keyboard[i] === 1) {
								this.pc += 2;
								pressed = true;
								break;
							}
						if(pressed === false) return;
						break;
					case 0x0015:
						this.delayTimer = this.v[x];
						break;
					case 0x0018:
						this.soundTimer = this.v[x];
						break;
					case 0x001E:
						this.i += this.v[x];
						if(this.i > 0xFF)
							this.i -= 256;
						break;
					case 0x0029:
						this.i = this.v[x] * 5;
						break;
					case 0x0033:
						let num = this.v[x];
						this.memory[this.i] = parseInt(num / 100);
						this.memory[this.i + 1] = parseInt(num / 10) % 10;
						this.memory[this.i + 2] = parseInt(num % 10);
						break;
					case 0x0055:
						for(let i = 0; i <= x ; i++)
							this.memory[this.i + i] = this.v[i];
						break;
					case 0x0065:
						for(let i = 0; i <= x; i++)
							this.v[i] = this.memory[this.i + i];
						break;
					default:
						console.error(`Invalid opcode ${opcode}`);
				}
				break;
			default:
				console.error(`Invalid opcode ${opcode}`);
		}
	}
}

module.exports = Chip8;
